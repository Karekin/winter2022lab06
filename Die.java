import java.util.Random;
public class Die{
	
	private int pips;
	private Random rng;
	
	public Die(){
		
		this.pips = 1;
		this.rng = new Random();
	}
	
	public int getPips(){
		return this.pips;
	}
	public Random getRng(){
		return this.rng;
	}
	
	public void roll(){
		this.pips = rng.nextInt(6) + 1;
	}
	
	public String toString(){
		
		return "" + this.pips;
	}
}