public class Board{
	
	private Die dice1;
	private Die dice2;
	private boolean[] closedTiles;
	
	public Board(){
		
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString(){
		
		String tiles = "Tiles (X = closed): ";
		for(int i = 0; i < this.closedTiles.length; i++){
			
			if(this.closedTiles[i]){
				tiles += "X ";
			}else{
				tiles += (i + 1) + " ";
			}
		}
		return tiles;
	}
	
	public boolean playATurn(){
		
		this.dice1.roll();
		this.dice2.roll();
		System.out.println("You rolled: " + dice1 + " " + dice2);
		int sum = this.dice1.getPips() + this.dice2.getPips();
		
		if(this.closedTiles[sum - 1]){
			System.out.println("This tile is already shut");
			return true;
		}else{
			System.out.println("Tile is now closed");
			this.closedTiles[sum -1] = true;
			return false;
		}
	}
}