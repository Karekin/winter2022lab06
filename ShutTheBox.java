import java.util.Scanner;
public class ShutTheBox{
	
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		int scoreP1 = 0;
		int scoreP2 = 0;
		while(true){
			System.out.println("Welcome to the game: Shut The Box! Test out your luck now with a short game.");
			Board game = new Board();
			boolean gameOver = false;
			while(gameOver == false){
				
				System.out.println("Player 1's turn");
				System.out.println(game);
				
				if(game.playATurn()){
					
					System.out.println("Player 2 wins!");
					gameOver = true;
					scoreP2++;
					break;
				}
				
					System.out.println("Player 2's turn");
					System.out.println(game);
					
				if(game.playATurn()){
					
					System.out.println("Player 1 wins!");
					gameOver = true;
					scoreP1++;
					break;
				}
			}
			String rematch;
			System.out.println("Would you like to play again? (Y/y = yes N/n = no)");
			while(true){
				
				rematch = s.next();
				if(rematch.equals("Y") || rematch.equals("y")){
					break;
				}else if(rematch.equals("N") || rematch.equals("n")){
					
					System.out.println("Thanks for playing!");
					break;
				}else{
					System.out.println("Please input Y,y,N,n !!!");
				}
			}
			if(rematch.equals("Y") || rematch.equals("y")){
				
				System.out.println("Have fun!");
				gameOver = false;
				continue;
			}
			
			System.out.println("");
			System.out.println("Player 1 has won: " + scoreP1 + " times. Player 2 has won: " + scoreP2 + " times.");
			break;
		}
	}
}